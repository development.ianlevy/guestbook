# guestbook
<hr>

Before I started creating, I did research by visiting tons of websites i found 
on [awwwards.com](https://www.awwwards.com/). One of these websites that I
really loved for its black and white thema, was [kwokyinmak.com](https://www.kwokyinmak.com/)

After browsing and exploring this website, I decided I wanted a similar 
guestbook as Kwok. So first thing I did was search for a bare-bones, simpel
type of guestbook (that i would design later). I searched on codepen, jsfiddle
and pastebin. And came up empty. I did find [this](https://codepen.io/Minilexikon/pen/JXYbNY) codesnippet, but that 
wasn't coded in a way like I wanted to do it.

But not finding a simple version of a guestbook didn't defeat me! I started to
break down the original kwokyinmak website, and tried to filter all useless
code out. Obviously keeping all the guestbook-related code. I have done this
type of 'stealing' before, with succes, so I had high hopes. Sadly enough I
found out pretty quickly how much code the website contained. A lot of 
Javascript and CSS that got mixed up and it confused the hell out of me.

I was very determined to get this guestbook in my website, because it was such
an orignal idea. Plus also very usefull to receive feedback on what visitors
thought about my website. But after wasting hours of my time, I gave up on it
(for this semester).

-IanLevy